use eframe::{egui_wgpu::RenderState, epaint::TextureId};
use wgpu::{util::DeviceExt, BlendComponent, Extent3d, TextureView};

use crate::wgpu_boilerplate::{
    allocate_texture, create_pipeline, register_tex_in_egui, PipelineType, Vertex,
};

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct ColorUniform {
    r: f32,
    g: f32,
    b: f32,
    _padding: u32,
}

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct DataBoundsUniform {
    w: f32,
    h: f32,
}

pub struct PointsRenderer {
    data_texture: wgpu::Texture,
    data_texture_view: wgpu::TextureView,
    data_bind_group: wgpu::BindGroup,

    target_texture_id: TextureId,
    target_texture_view: wgpu::TextureView,
    target2_texture_id: TextureId,
    target2_texture_view: wgpu::TextureView,
    replacing_pipeline: wgpu::RenderPipeline,
    subtracting_pipeline: wgpu::RenderPipeline,
    render_state: RenderState,

    vertex_buffer: wgpu::Buffer,

    color_buffer: wgpu::Buffer,

    data_bounds_buffer: wgpu::Buffer,
}

const SQUARE_VERTICES: &[Vertex] = &[
    Vertex {
        position: [-1.0, 1.0],
        uv: [0.0, 0.0],
    },
    Vertex {
        position: [-1.0, -1.0],
        uv: [0.0, 1.0],
    },
    Vertex {
        position: [1.0, 1.0],
        uv: [1.0, 0.0],
    },
    Vertex {
        position: [-1.0, -1.0],
        uv: [0.0, 1.0],
    },
    Vertex {
        position: [1.0, -1.0],
        uv: [1.0, 1.0],
    },
    Vertex {
        position: [1.0, 1.0],
        uv: [1.0, 0.0],
    },
];

impl PointsRenderer {
    pub fn texture_id(&self) -> Vec<TextureId> {
        vec![self.target_texture_id]
    }

    pub(crate) fn new(rs: &RenderState) -> PointsRenderer {
        let (_target_tex, target_tex_view) = allocate_texture(
            rs,
            1024,
            1024,
            wgpu::TextureFormat::Rgba16Float,
            wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
        );
        let (data_tex, data_tex_view) = allocate_texture(
            rs,
            1024,
            1024,
            wgpu::TextureFormat::R32Float,
            wgpu::TextureUsages::COPY_DST | wgpu::TextureUsages::TEXTURE_BINDING,
        );
        let (_target_tex2, target2_texture_view) = allocate_texture(
            rs,
            1024,
            1024,
            wgpu::TextureFormat::Rgba16Float,
            wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
        );
        let tex_id = register_tex_in_egui(&target_tex_view, rs);
        let target2_texture_id = register_tex_in_egui(&target2_texture_view, rs);

        let data_tex_sampler = rs.device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        let data_bind_group_layout =
            rs.device
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    entries: &[
                        // Data tex
                        wgpu::BindGroupLayoutEntry {
                            binding: 0,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Texture {
                                multisampled: false,
                                view_dimension: wgpu::TextureViewDimension::D2,
                                sample_type: wgpu::TextureSampleType::Float { filterable: false },
                            },
                            count: None,
                        },
                        // Data tex sampler
                        wgpu::BindGroupLayoutEntry {
                            binding: 1,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::NonFiltering),
                            count: None,
                        },
                        // Color
                        wgpu::BindGroupLayoutEntry {
                            binding: 2,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                        // Data bounds
                        wgpu::BindGroupLayoutEntry {
                            binding: 3,
                            visibility: wgpu::ShaderStages::FRAGMENT,
                            ty: wgpu::BindingType::Buffer {
                                ty: wgpu::BufferBindingType::Uniform,
                                has_dynamic_offset: false,
                                min_binding_size: None,
                            },
                            count: None,
                        },
                    ],
                    label: Some("texture_bind_group_layout"),
                });
        let color_buffer = rs
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Color Uniform Buffer"),
                contents: bytemuck::cast_slice(&[ColorUniform {
                    r: 0.2,
                    g: 0.4,
                    b: 1.0,
                    _padding: 0,
                }]),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });

        let data_bounds_buffer = rs
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Color Uniform Buffer"),
                contents: bytemuck::cast_slice(&[DataBoundsUniform { w: 1.0, h: 1.0 }]),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });

        let data_bind_group = rs.device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &data_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&data_tex_view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&data_tex_sampler),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: color_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 3,
                    resource: data_bounds_buffer.as_entire_binding(),
                },
            ],
            label: Some("data_bind_group"),
        });

        let pipeline_layout = rs
            .device
            .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("pointer pipeline layout"),
                bind_group_layouts: &[&data_bind_group_layout],
                push_constant_ranges: &[],
            });
        let shader = rs
            .device
            .create_shader_module(wgpu::include_wgsl!("shader.wgsl"));
        let replacing_pipeline =
            create_pipeline(rs, &pipeline_layout, &shader, PipelineType::Replacing);
        let subtracting_pipeline =
            create_pipeline(rs, &pipeline_layout, &shader, PipelineType::Subtracting);
        let vertex_buffer = rs
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Vertex Buffer"),
                contents: bytemuck::cast_slice(SQUARE_VERTICES),
                usage: wgpu::BufferUsages::VERTEX,
            });

        rs.queue.write_texture(
            // Tells wgpu where to copy the pixel data
            wgpu::ImageCopyTexture {
                texture: &data_tex,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            },
            // The actual pixel data
            bytemuck::cast_slice(&sample_tex()),
            // The layout of the texture
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: std::num::NonZeroU32::new(4 * 1024),
                rows_per_image: std::num::NonZeroU32::new(1024),
            },
            Extent3d {
                width: 1024,
                height: 1024,
                depth_or_array_layers: 1,
            },
        );

        let result = Self {
            target_texture_id: tex_id,
            target_texture_view: target_tex_view,
            data_texture: data_tex,
            data_texture_view: data_tex_view,
            data_bind_group,
            replacing_pipeline,
            subtracting_pipeline,
            render_state: rs.clone(),
            vertex_buffer,
            color_buffer,
            data_bounds_buffer,
            target2_texture_view,
            target2_texture_id,
        };
        result.change_color((0.2, 0.4, 0.1), (0.0, 1.0, 0.0), (1.0, 1.0), (0.4, 0.3));
        result
    }

    pub fn change_color(
        &self,
        color: (f64, f64, f64),
        color2: (f64, f64, f64),
        bounds: (f32, f32),
        bounds2: (f32, f32),
    ) {
        self.draw_texture(
            &self.target_texture_view,
            &self.replacing_pipeline,
            color,
            bounds,
        );
        self.draw_texture(
            &self.target_texture_view,
            &self.subtracting_pipeline,
            color2,
            bounds2,
        );
    }

    pub fn draw_texture(
        &self,
        texture_view: &wgpu::TextureView,
        pipeline: &wgpu::RenderPipeline,
        color: (f64, f64, f64),
        bounds: (f32, f32),
    ) {
        self.render_state.queue.write_buffer(
            &self.color_buffer,
            0,
            bytemuck::cast_slice(&[ColorUniform {
                r: color.0 as f32,
                g: color.1 as f32,
                b: color.2 as f32,
                _padding: 0,
            }]),
        );
        self.render_state.queue.write_buffer(
            &self.data_bounds_buffer,
            0,
            bytemuck::cast_slice(&[DataBoundsUniform {
                w: bounds.0,
                h: bounds.1,
            }]),
        );
        let mut encoder =
            self.render_state
                .device
                .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("data displayer"),
                });

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("data displayer"),
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: texture_view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Load,
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(pipeline);
            render_pass.set_bind_group(0, &self.data_bind_group, &[]);
            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
            render_pass.draw(0..(SQUARE_VERTICES.len() as u32), 0..1);
        }

        self.render_state
            .queue
            .submit(std::iter::once(encoder.finish()));
    }
}

fn sample_tex() -> Vec<f32> {
    (0..1024)
        .flat_map(|y| {
            (0..1024).map(move |x| {
                (y as f32 / 10.0).sin() * 100.0 + (x as f32 / 10.0).cos() * 100.0 + 0.5
            })
        })
        .collect()
}
