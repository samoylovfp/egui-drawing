//! Boring wgpu stuff

use eframe::{egui_wgpu::RenderState, epaint::TextureId};
use wgpu::{
    MultisampleState, PrimitiveState, RenderPipelineDescriptor, Texture, TextureFormat, TextureView,
};

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Vertex {
    pub position: [f32; 2],
    pub uv: [f32; 2],
}

impl<'a> Vertex {
    const ATTRIBS: [wgpu::VertexAttribute; 2] =
        wgpu::vertex_attr_array![0 => Float32x2, 1 => Float32x2];

    fn layout() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &Vertex::ATTRIBS,
        }
    }
}

pub enum PipelineType {
    Replacing,
    Subtracting,
}

pub fn create_pipeline(
    rs: &RenderState,
    pipeline_layout: &wgpu::PipelineLayout,
    shader: &wgpu::ShaderModule,
    pipeline_type: PipelineType,
) -> wgpu::RenderPipeline {
    let blend_state = match pipeline_type {
        PipelineType::Replacing => wgpu::BlendState::REPLACE,
        PipelineType::Subtracting => wgpu::BlendState {
            color: wgpu::BlendComponent {
                src_factor: wgpu::BlendFactor::Src,
                dst_factor: wgpu::BlendFactor::Dst,
                operation: wgpu::BlendOperation::ReverseSubtract,
            },
            alpha: wgpu::BlendComponent {
                src_factor: wgpu::BlendFactor::Src,
                dst_factor: wgpu::BlendFactor::Dst,
                operation: wgpu::BlendOperation::Max,
            },
        },
    };
    let pipeline = rs.device.create_render_pipeline(&RenderPipelineDescriptor {
        label: Some("pointer pipeline"),
        layout: Some(pipeline_layout),
        vertex: wgpu::VertexState {
            module: shader,
            entry_point: "vs_main",
            buffers: &[Vertex::layout()],
        },
        fragment: Some(wgpu::FragmentState {
            module: shader,
            entry_point: "fs_main",
            targets: &[Some(wgpu::ColorTargetState {
                format: TextureFormat::Rgba16Float,
                blend: Some(blend_state),
                write_mask: wgpu::ColorWrites::ALL,
            })],
        }),
        primitive: PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            cull_mode: None,
            unclipped_depth: false,
            polygon_mode: wgpu::PolygonMode::Fill,
            conservative: false,
        },
        depth_stencil: None,
        multisample: MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
    });
    pipeline
}

pub fn allocate_texture(
    rs: &RenderState,
    width: u32,
    height: u32,
    format: TextureFormat,
    flags: wgpu::TextureUsages,
) -> (Texture, TextureView) {
    let tex = rs.device.create_texture(&wgpu::TextureDescriptor {
        label: Some("green texture"),
        size: wgpu::Extent3d {
            width,
            height,
            depth_or_array_layers: 1,
        },
        format,
        usage: flags,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
    });
    let view = tex.create_view(&wgpu::TextureViewDescriptor::default());
    (tex, view)
}

pub fn register_tex_in_egui(view: &TextureView, rs: &RenderState) -> TextureId {
    let mut render_pass = rs.egui_rpass.write();

    render_pass.register_native_texture(&rs.device, view, wgpu::FilterMode::Nearest)
}
