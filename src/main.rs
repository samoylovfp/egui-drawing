mod wgpu_boilerplate;
mod wgpu_shenanigans;

use eframe::{
    egui::{
        self,
        plot::{PlotImage, Value},
    },
    emath::Vec2,
};
use wgpu_shenanigans::PointsRenderer;

fn main() {
    let options = eframe::NativeOptions::default();
    eframe::run_native(
        "egui wgpu drawing",
        options,
        Box::new(|cc| {
            Box::new(MyApp {
                points_renderer: PointsRenderer::new(cc.render_state.as_ref().unwrap()),
                color: (0.0, 0.2, 0.0),
                data_bounds: (1.0, 1.0),
                color2: (0.0, 1.0, 0.0),
                data_bounds2: (0.3, 0.4),
            })
        }),
    );
}

struct MyApp {
    points_renderer: PointsRenderer,
    color: (f64, f64, f64),
    data_bounds: (f32, f32),
    color2: (f64, f64, f64),
    data_bounds2: (f32, f32),
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.horizontal(|ui| {
                let color_group = ui.vertical(|ui| {
                    ui.add(egui::Slider::new(&mut self.color.0, 0.0..=1.0))
                        .union(ui.add(egui::Slider::new(&mut self.color.1, 0.0..=1.0)))
                        .union(ui.add(egui::Slider::new(&mut self.color.2, 0.0..=1.0)))
                });
                let data_bounds_group = ui.vertical(|ui| {
                    ui.add(egui::Slider::new(&mut self.data_bounds.0, 0.0..=1.0))
                        .union(ui.add(egui::Slider::new(&mut self.data_bounds.1, 0.0..=1.0)))
                });
                if color_group.inner.changed() || data_bounds_group.inner.changed() {
                    self.points_renderer.change_color(
                        self.color,
                        self.color2,
                        self.data_bounds,
                        self.data_bounds2,
                    );
                }
            });
            ui.label("Ctrl + Scroll wheel to zoom");
            egui::plot::Plot::new("plot")
                .view_aspect(1.0)
                .show(ui, |ui| {
                    for (i, tex_id) in self.points_renderer.texture_id().into_iter().enumerate() {
                        ui.image(PlotImage::new(
                            tex_id,
                            Value::new(i as f64 / 3.0, 0.0),
                            Vec2::new(1.0, 1.0),
                        ))
                    }
                });
        });
    }
}
