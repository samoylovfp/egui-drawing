// Vertex shader


struct VertexInput {
    @location(0) position: vec2<f32>,
    @location(1) ui: vec2<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) ui: vec2<f32>,
};

@vertex
fn vs_main(
    model: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    out.ui = model.ui;
    out.clip_position = vec4<f32>(model.position, 0.0, 1.0);
    return out;
}

// Fragment shader

@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0)@binding(1)
var s_diffuse: sampler;
@group(0)@binding(2)
var<uniform> color: vec3<f32>;
@group(0)@binding(3)
var<uniform> bounds: vec2<f32>;

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    var value = textureSample(t_diffuse, s_diffuse, in.ui * bounds).r / 400.0 + 0.5;
    return vec4<f32>(value * color[0], value * color[1], value * color[2], value);
}